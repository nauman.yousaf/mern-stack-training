//access static file via express

const express = require('express'); //import module
// the above constant will return a function called express();

const path = require('path'); //path module. its autility module 

const Joi = require('joi'); //path module. its autility module 


const app = express();
// this express method retrun an object which has a bunch of methods

const bodyParser = require('body-parser'); //body parser module need to be download for parsing form data.

// __dirname will give the directory path where the current file is located.
app.use('/nauman',express.static(path.join(__dirname,'static'))) ;// whenever we use this use() mean we are going to use a middleware. first aurgument is the alias

// __dirname will give the directory path where the current file is located.
app.use(express.urlencoded({
    extended: false,
}));
app.use(express.json());
app.get('/',(req,res)=>{
    res.sendFile(path.join(__dirname, 'static', 'contact.html'));
});

app.post('/',(req,res)=>{
    // console.log(req.body);
    const schema = Joi.object().keys({
        firstname : Joi.string().trim().required(),
        lastname : Joi.string().trim().required(),
        email : Joi.string().trim().email().required(),
        password : Joi.string().trim().min(3).max(8).required(),
    });
    const { error, value }  = schema.validate(req.body);

    if(error){
        console.log(error);
        res.send('Error occur');
    
    }else{
        console.log(value);
        res.send('successfull');
    }
    
});

app.listen('3000');