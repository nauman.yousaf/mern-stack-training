const express = require('express'); //import module
// the above constant will return a function called express();
const bodyParser = require('body-parser'); //path module. its autility module 
const app = express();
app.use(express.json());
app.use((req,res,next)=>{ //Middleware for all routes
    console.log(req.url,req.method);
    next();
res.send('middleware');
});

app.use('/example',(req,res,next)=>{ //Middleware for only example route
    console.log(req.url,req.method);
    next();
res.send('middleware');
});
app.listen('3000');