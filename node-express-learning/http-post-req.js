//access static file via express

const express = require('express'); //import module
// the above constant will return a function called express();

const path = require('path'); //path module. its autility module 

const app = express();
// this express method retrun an object which has a bunch of methods

const bodyParser = require('body-parser'); //body parser module need to be download for parsing form data.

// __dirname will give the directory path where the current file is located.
app.use('/nauman',express.static(path.join(__dirname,'static'))) ;// whenever we use this use() mean we are going to use a middleware. first aurgument is the alias

// __dirname will give the directory path where the current file is located.
app.use(bodyParser.urlencoded({
    extended: false,
}));
app.get('/',(req,res)=>{
    res.sendFile(path.join(__dirname, 'static', 'contact.html'));
});

app.post('/',(req,res)=>{
    console.log(req.body);
    res.send("Successfully send data");
});

app.listen('3000');