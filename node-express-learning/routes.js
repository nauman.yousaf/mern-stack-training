//access static file via express

const express = require('express'); //import module
// the above constant will return a function called express();

const path = require('path'); //path module. its autility module 

const app = express();
// this express method retrun an object which has a bunch of methods

// __dirname will give the directory path where the current file is located.
app.use('/public',express.static(path.join(__dirname,'static'))) ;// whenever we use this use() mean we are going to use a middleware. first aurgument is the alias

app.set('view engine', 'ejs');
const people = require('./routes/people');

app.use('/people', people);



app.listen('3000');