const http      = require('http');
const fs = require('fs');
const server = http.createServer((req,res)=>{
    // const readStream = fs.createReadStream('./static/example.html');
    // res.writeHead(200,{'Content-type' : 'text/html'});
    // readStream.pipe(res);


    // const readStreamJson = fs.createReadStream('./static/example.json');
    // res.writeHead(200,{'Content-type' : 'application/json'});
    // readStreamJson.pipe(res);

    const readStreamImg = fs.createReadStream('./static/download.png');
    res.writeHead(200,{'Content-type' : 'image/png'});
    readStreamImg.pipe(res);


}).listen('3000');
