const fs = require('fs');
fs.writeFile('Example.txt', "This is an example file", (err)=>{
    if(err){
        console.log(err);
    }else{
        console.log("File created successfully");
        fs.readFile("Example.txt",'utf8',(err,file)=>{
            if(err)
                console.log(err);
            else
                console.log(file);
        })
    }
});

fs.rename('Example.txt','nomy.txt',(err)=>{
    if(err)
        console.log(err);
    else
        console.log("File rename successfully");
});