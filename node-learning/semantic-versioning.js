// semantic-versioning.js

// Versioning detals of package check the following example:


// "lodash": "^4.17.21" // 4 is the major version 17 is the minor version 21 is the patch version
//  Patch = Bug Fix in a previous release
//  Minor = Added new functionality and might depricate some old functionality but still you can use it. So its non breaking update change 
//  Major = This does not support any old version. Breaking Change Update
// ^ prefix:
//  in package version number means that you do not want any major update but if there is any minor and patch update
// go ahead and please update
// ~ prefix:
//  in package version number ~ sign prefix means you only want patch update
// If there is not prefix sign (^ or ~)in package version that means you do not want any update and want to stick with the current version
